angular.module("UnisegurosApp")
	.controller("detallesServiciosController", function ($scope, $http, $location, direccionFactory, redessocialesFactory, metaFactory, nosotrosFactory, productosFactory, detalleserviciosFactory,otrosserviciosFactory ) {
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu3").addClass("subtitulo-menu2")
		$scope.slug = $("#slug").val();
		$scope.slug_base = $("#slug_base").val();
		//console.log($scope.slug_base);
		//console.log($scope.slug);
		$scope.consultar_servicios = function () {
			detalleserviciosFactory.asignar_valores("", $scope.slug, $scope.slug_base, $scope.base_url)
			detalleserviciosFactory.cargar_servicios(function (data) {
				//console.log(data);
				$scope.servicio_base = data.servicio_base;
				$scope.tipo_servicio = data.tipo_servicio;
				$scope.detalles_servicios = data.detalles_servicios;
				$scope.orden_detalles_servicios = $scope.detalles_servicios[0].orden;
				$scope.iniciar_detalle_seleccionado($scope.detalles_servicios[0])
				$scope.consultar_otros_tipo_servicios($scope.tipo_servicio.id,$scope.servicio_base.id);


			});
		}

		$scope.iniciar_detalle_seleccionado = function (data) {
			//console.log(data);
			$scope.detalles_iniciales = data;
			$scope.titulo_detalle_servicio = $scope.detalles_iniciales.titulo;
			$scope.descripcion_detalle_servicio = $scope.detalles_iniciales.descripcion;
			$("#descripcion_detalle_servicio").empty();
			$("#descripcion_detalle_servicio").hide().append($scope.descripcion_detalle_servicio).fadeIn(1000);;

		}
		$scope.detalle_seleccionado = function (event) {
			$scope.titulo_detalle_servicio = []
			$scope.descripcion_detalle_servicio = []

			var caja = event.currentTarget.id;
			var atributos = $("#" + caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			//console.log(arreglo_atributos);

			$scope.titulo_detalle_servicio = arreglo_atributos[0];
			$scope.descripcion_detalle_servicio = arreglo_atributos[1];
			$("#descripcion_detalle_servicio").empty();
			$("#descripcion_detalle_servicio").hide().append($scope.descripcion_detalle_servicio).fadeIn(1000);
			
		}
		$scope.consultar_otros_tipo_servicios = function(tipo_servicio,servicio_base){
			$scope.id_tipo_servicio = tipo_servicio;
			$scope.id_servicio = servicio_base;
			otrosserviciosFactory.asignar_valores($scope.id_servicio,$scope.id_tipo_servicio)
			otrosserviciosFactory.cargar_servicios(function(data){
				//console.log(data);
				$scope.otros_servicios=data;


			});
		}
		$scope.consultar_algunos_productos = function(){
			productosFactory.asignar_valores("","",$scope.base_url)
			productosFactory.cargar_productos_servicios(function(data){
				
				$scope.algunos_productos=data;
				//console.log($scope.algunos_productos)

			});
		}

		$scope.activarAcordeon = function(index){
			$(".btn-link-productos").removeClass("acordion-text-selected");
			$("#acordeon"+index).addClass("acordion-text-selected");
		}
		
		$scope.consultar_algunos_productos();
		$scope.consultar_servicios();
	});