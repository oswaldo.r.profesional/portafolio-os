angular.module("UnisegurosApp")
	.controller("serviciosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,serviciosFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu3").addClass("subtitulo-menu2")
		$scope.consultar_servicios = function(){
			serviciosFactory.asignar_valores("","",$scope.base_url)
			serviciosFactory.cargar_servicios_principales(function(data){
				$scope.servicios=data;
				//console.log($scope.servicios)
			});
		}
		$scope.consultar_servicios();
	});