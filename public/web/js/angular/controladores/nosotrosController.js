angular.module("UnisegurosApp")
	.controller("nosotrosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory,cargaPdfNotificacionesFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu1").addClass("subtitulo-menu2")
		$scope.limit = 3
		$scope.start =  6
		$scope.limit_ini =  6
		$scope.start_init =  0
		//--Quienes somos
		
		$scope.pdf  = {
									'id' : '',
									'descripcion' : '',
									'descripcion_sin_html' : '',
									'estatus' : '',
									'id':'',
									'id_categoria' : '',
									'ruta':'',
									'titulo':''
		}
		$scope.sub_pdf  = []
		//---------------------------
		$scope.mostrar_contenido = function(opcion){
			$("#modal-2").modal("show")
			var lista = "";
			if(opcion==1){
				lista = $(".lista-mision ul").html()
				$( ".lista-mision ul" ).each(function( index ) {
					lista+=$( this ).html();
				});

				$("#ul-modal").html(lista);
				$scope.titulo_modal = "Misión, contamos con:"
			}else if(opcion==2){
				lista = $(".lista-vision ul").html()
				$( ".lista-vision ul" ).each(function( index ) {
					lista+=$( this ).html();
				});

				$("#ul-modal").html(lista);
				$scope.titulo_modal = "Visión, contando con:"
			}
		}
		//---------------------------
		$scope.consultar_nosotros = function(){
			nosotrosFactory.asignar_valores($scope.id_idioma,"",$scope.base_url)
			nosotrosFactory.cargar_nosotros(function(data){
				$scope.nosotros=data;
				//console.log($scope.nosotros)
			})	
		}
		$scope.consultar_directiva = function(){
			directivaFactory.asignar_valores("","",$scope.base_url)
			directivaFactory.cargar_directiva(function(data){
				$scope.directiva=data;
				//console.log($scope.directiva);	
				setTimeout(function(){
					//----------------
					$(".carousel-directiva").each(function() {
			            $(this).owlCarousel($.extend({
				                navigation: !1,
				                pagination: !0,
				                autoPlay: 3e3,
				                items: 3,
				                navigationText: ['<span class="arrows arrows-arrows-slim-left"></span>', '<span class="arrows arrows-arrows-slim-right"></span>']
			            }, $(this).data("carousel-options")))
		        	})	
					//----------------
				},500)
						
			});
		}
		$scope.consultar_reaseguradoras = function(){
			reaseguradorasFactory.asignar_valores("","",$scope.base_url)
			reaseguradorasFactory.cargar_reaseguradoras(function(data){
				$scope.reaseguradoras=data;
				//console.log($scope.reaseguradoras);	
				//---------------------------------
				setTimeout(function(){
					$(".carousel-reaseguradoras").each(function() {
			            $(this).owlCarousel($.extend({
			                navigation: !1,
			                pagination: !1,
			                autoPlay: 3e3,
			                items: 4,
			                navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
			            }, $(this).data("carousel-options")))
			        })
			    },500)        
		        //-----------------------------------			
			});
		}
		/*
		*	consultar_pdf
		*/
		$scope.consultar_pdf = function(){
			cargaPdfNotificacionesFactory.asignar_valores("","31",$scope.base_url,$scope.limit_ini,$scope.start_init)
			cargaPdfNotificacionesFactory.cargar_pdf(function(data){
				$scope.pdf=data;
				console.log($scope.pdf);				
			});
		}
		/***/
		$scope.cargar_mas_subnotificaciones = function(){
			cargaPdfNotificacionesFactory.asignar_valores("","31",$scope.base_url,$scope.limit,$scope.start)
			cargaPdfNotificacionesFactory.cargar_pdf(function(data){
				console.log(objectLength(data));
				//----------------------------------------
				if (data === null || data == "null" || data=="" || objectLength(data)==0) {
					mensaje_alerta("#campo_mensaje_notificaciones","Todas las notificaciones fueron cargadas!","alert-warning");
				} else {
					for (i in data) {
						$scope.sub_pdf.push(data[i]);
					}
					$scope.start += 3;
				}
				//----------------------------------------
			});
		}
		//---
		//---------------------------
		$scope.consultar_nosotros()
		$scope.consultar_directiva()
		$scope.consultar_reaseguradoras();
		$scope.consultar_pdf();
	});