angular.module("UnisegurosApp")
	.controller("inicioController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,productoshomeFactory,servicioshomeFactory,cargaPdfFactory,cuentaBancariaFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$scope.idioma = 1
		//--Quienes somos
		$scope.nosotros  = {
									'id' : '',
									'titulo' : '',
									'descripcion' : '',
									'id_idioma' : '',
									'icono':'',
									'idioma' : '',
									'id_imagen':'',
		}
		$scope.contactos = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
		$scope.direccion_mapas = []
		$scope.direccion_mapas_telefono = []
		$scope.mapa1 = "assets/images/archivos/mapas/mapas1.png"
		//---------------------------
		$scope.consultar_nosotros = function(){
			nosotrosFactory.asignar_valores($scope.id_idioma,"",$scope.base_url)
			nosotrosFactory.cargar_nosotros(function(data){
				$scope.nosotros=data;
				//console.log($scope.nosotros)
			})	
		}
		//--
		$scope.validar_guardar = function(){
			if($scope.contactos.nombres==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar sus nombres", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter their names", "alert-danger");

				return false;
			}else
			if($("#form-email").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar una dirección de correo v&aacute;lida", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid email direction", "alert-danger");
				return false;
			}
			else
			if($("#form-tlf").val()==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un número de teléfono válido", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter valid telephone number", "alert-danger");
				return false;
			}
			else if($scope.contactos.mensaje==""){
				if($scope.idioma == "1")
					mensaje_alerta("#campo_mensaje_clientes","Debe ingresar un mensaje", "alert-danger");
				else
					mensaje_alerta("#campo_mensaje_clientes","You must enter a message", "alert-danger");
				return false;
			}else
				return true;
		}
		//--
		//--Registrar contactos
		$scope.registrar_contactos = function(){
			if($scope.validar_guardar()==true){
				uploader_reg("#campo_mensaje_clientes",".campos-form");
				$http.post($scope.base_url+"/WebContactos/registrarContactos",{
					'nombres': $scope.contactos.nombres,
					'email': $scope.contactos.email,
					'telefono': $scope.contactos.telefono,
					'mensaje': $scope.contactos.mensaje
				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					if($scope.mensajes.mensaje == "registro_procesado"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Gracias por tu tiempo, tu mensaje ha sido enviado!","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","Thank you for your time, your message has been sent!","alert-success");
						$scope.limpiar_cajas();
					}else if($scope.mensajes.mensaje =="existe"){
						if($scope.idioma == "1")
							mensaje_alerta("#campo_mensaje_clientes","Ya fue registrado un contacto con esa dirección de email","alert-success");
						else
							mensaje_alerta("#campo_mensaje_clientes","A contact has already been registered with that email address","alert-success");
					}
					else{
						mensaje_alerta("#campo_mensaje_clientes","Ocurrió un error inesperado","alert-danger");

					}
					desbloquear_pantalla("#campo_mensaje_clientes",".campos-form")
				}).error(function(data,estatus){
					showErrorMessage("Ocurrió un error inesperado:"+data);
				})
			}
		}

		//--Limpiar cajas
		$scope.limpiar_cajas = function(){
			$scope.contactos = {
									"id":"",
									"nombres":"",
									"email":"",
									"telefono":"",
									"mensaje":""
			}
		}
		//-- consultar_direccion
		$scope.consultar_direccion = function(){
			direccionFactory.asignar_valores("","",$scope.base_url)
			direccionFactory.cargar_direccion(function(data){
				$scope.direccion=data;
				//console.log($scope.direccion);
				$scope.direccion_mapas = $scope.direccion[0]
			  	$scope.direccion_mapas_telefono = $scope.direccion_mapas.telefono
			  	$("#direccion_mapas").html($scope.direccion_mapas.iframe)
			  	//---------------------------------------------------------------
			  	var latitud= $scope.direccion_mapas.latitud;  
				var longitud= $scope.direccion_mapas.longitud;
				var numero = 0;
				//google.maps.event.addDomListener(window, 'load', initialize_standar(latitud,longitud,numero));
				setTimeout(function(){
					$("#listaLocalizacion0").addClass("alert-success")
				},5000)
			});
		}
		/*
		*	preSeleccionarRenglon
		*/
		$scope.preSeleccionarRenglon = function(indice){
			//---
			$( ".lista-localizacion" ).each(function( index ) {
			  	if(!$(this).hasClass("alert-success")){
			  		$(this).removeClass("alert-info")
				}
			});
			//---
			if(!$(this).hasClass("alert-success")){
		  		$("#listaLocalizacion"+indice).addClass("alert-info")
		  	}
		  	//---	
		}
		/*
		*	seleccionaRenglon
		*/
		$scope.seleccionaRenglon = function(indice){
			//---
			$( ".lista-localizacion" ).each(function( index ) {
				$(this).removeClass("alert-info").removeClass("alert-success")
			});
			//---
		  	$("#listaLocalizacion"+indice).addClass("alert-success")
		  	$scope.direccion_mapas = $scope.direccion[indice]
		  	$scope.direccion_mapas_telefono = $scope.direccion_mapas.telefono
		  	//---------------------------------------------------------------
		  	var latitud= $scope.direccion_mapas.latitud;  
			var longitud= $scope.direccion_mapas.longitud;
			var numero = 0;
			//google.maps.event.addDomListener(window, 'load', initialize_standar(latitud,longitud,numero));
		  	//---------------------------------------------------------------
		  	//console.log($scope.direccion_mapas)
		}
		/*
		*	desSeleccionarRenglon
		*/
		$scope.desSeleccionarRenglon = function(indice){
			$("#listaLocalizacion"+indice).removeClass("alert-info")
		}
		//--
		$scope.consultar_productos = function(){
			productoshomeFactory.asignar_valores("","",$scope.base_url)
			productoshomeFactory.cargar_productos_home(function(data){
				$scope.productos=data;
				//console.log($scope.productos)

			});
		}
		$scope.consultar_servicios = function(){
			servicioshomeFactory.asignar_valores("","",$scope.base_url)
			servicioshomeFactory.cargar_servicios_home(function(data){
				$scope.servicios=data;
				$scope.tservicios=data.tservicios;
				//console.log($scope.servicios)
				//console.log($scope.tservicios)

			});
		}

		/*
		*	Carga de cuentas bancarias
		*/
		$scope.consultar_banco = function(){
			cuentaBancariaFactory.asignar_valores("","",$scope.base_url)
			cuentaBancariaFactory.cargar_banco(function(data){
				$scope.banco=data;
				//console.log($scope.banco);				
			});
		}
		/*
		*	consultar_pdf
		*/
		$scope.consultar_pdf = function(){
			cargaPdfFactory.asignar_valores("","30",$scope.base_url)
			cargaPdfFactory.cargar_banco(function(data){
				$scope.pdf=data[0];
				//console.log($scope.pdf);				
			});
		}
		//--
		$scope.consultar_banco()
		$scope.consultar_pdf()
		//---------------------------
		$scope.consultar_servicios();
		$scope.consultar_productos();
		$scope.consultar_nosotros()
		$scope.consultar_direccion()
		//---------------------------
	});