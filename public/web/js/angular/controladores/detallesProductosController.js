angular.module("UnisegurosApp")
	.controller("detallesProductosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory,detalleproductosFactory,otrosproductosFactory,serviciosFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu2").addClass("subtitulo-menu2")
		//--				
		$scope.slug  = $("#slug").val();
		$scope.slug_base  = $("#slug_base").val();
		//console.log($scope.slug);
		//console.log($scope.slug_base);
		$scope.consultar_productos = function(){
			detalleproductosFactory.asignar_valores("",$scope.slug,$scope.slug_base,$scope.base_url)
			detalleproductosFactory.cargar_productos(function(data){
				$scope.producto_base=data.producto_base;
				$scope.tipo_producto=data.tipo_producto;
				$scope.detalles_productos=data.detalles_productos;
				//Creo una variable con el orde inicial '1'
				$scope.orden_detalles_productos = $scope.detalles_productos[0].orden;
				$scope.iniciar_detalle_seleccionado($scope.detalles_productos[0])
				$scope.consultar_otros_tipo_productos($scope.tipo_producto.id,$scope.producto_base.id);

			});
		}
		$scope.iniciar_detalle_seleccionado = function(data){
			//console.log(data);
			$scope.detalles_iniciales = data;
			$scope.titulo_detalle_producto = $scope.detalles_iniciales.titulo;
			$scope.descripcion_detalle_producto = $scope.detalles_iniciales.descripcion;
			$("#descripcion_detalle_producto").empty();
			$("#descripcion_detalle_producto").hide().append($scope.descripcion_detalle_producto).fadeIn(1000);
		}
		$scope.detalle_seleccionado = function(event){
					$scope.titulo_detalle_producto = []
					$scope.descripcion_detalle_producto = []
					
                    var caja = event.currentTarget.id;
                    var atributos = $("#"+caja).attr("data");
                    var arreglo_atributos = atributos.split("|");
					//console.log(arreglo_atributos);

				   $scope.titulo_detalle_producto = arreglo_atributos[0];
				   $scope.descripcion_detalle_producto = arreglo_atributos[1];
				   $("#descripcion_detalle_producto").empty();
				   $("#descripcion_detalle_producto").hide().append($scope.descripcion_detalle_producto).fadeIn(1000);  	

		}
		$scope.consultar_otros_tipo_productos = function(tipo_producto,producto_base){
			$scope.id_tipo_producto = tipo_producto;
			$scope.id_producto = producto_base;
			otrosproductosFactory.asignar_valores($scope.id_producto,$scope.id_tipo_producto)
			otrosproductosFactory.cargar_productos(function(data){
				//console.log(data);
				$scope.otros_productos=data;
				var keys = Object.keys(data);
				$scope.lenOtrosProductos = keys.length;
			});
		}
		
		$scope.consultar_algunos_servicios = function(){
			serviciosFactory.asignar_valores("","",$scope.base_url)
			serviciosFactory.cargar_servicios(function(data){
				
				$scope.algunos_servicios=data;
				//console.log($scope.algunos_servicios)

			});
		}

		$scope.activarAcordeon = function(index){
			$(".btn-link-productos").removeClass("acordion-text-selected");
			$("#acordeon"+index).addClass("acordion-text-selected");
		}

		$scope.consultar_algunos_servicios();
		$scope.consultar_productos();

	});