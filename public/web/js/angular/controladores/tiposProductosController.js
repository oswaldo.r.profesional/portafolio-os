angular.module("UnisegurosApp")
	.controller("tiposProductosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory,tproductosFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu2").addClass("subtitulo-menu2")
		//--				
		$scope.slug_producto  = $("#slug_producto").val();
		//console.log($scope.slug_producto);
		
		$scope.consultar_productos = function(){
			tproductosFactory.asignar_valores("","",$scope.slug_producto,$scope.base_url)
			tproductosFactory.cargar_productos(function(data){
				console.log(data)
				$scope.titulo=data.titulo;
				$scope.ruta=data.ruta;
				$scope.slug_base=data.slug;
				$scope.tproductos=data.tproductos;
				$scope.otros_productos=data.otros_productos;
				//console.log(data)

			});
		}
		$scope.consultar_productos();

	});