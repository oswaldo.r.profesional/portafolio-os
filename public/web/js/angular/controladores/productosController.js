angular.module("UnisegurosApp")
	.controller("productosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory,productosFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu2").addClass("subtitulo-menu2")
		//--				

		$scope.consultar_productos = function(){
			productosFactory.asignar_valores("","",$scope.base_url)
			productosFactory.cargar_productos(function(data){
				$scope.productos=data;
				//console.log($scope.productos)

			});
		}
		$scope.consultar_productos();
	});